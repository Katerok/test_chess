import QtQuick 2.2

Image {
    id: root
    width: 484; height: 484
    source: "chessboard.png"

    Grid {
        objectName: "squares_grid"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.fill: parent
        anchors.margins: 2
        columns: 8
        rows: 8

        Component {
            id: square_component
            Rectangle {
                id: square
                objectName: "square"
                color: "transparent"
                border.color: "transparent"
                width: 60
                height: 60
                border.width: 1
                radius: 10

                signal qmlSquareClicked()

                Image {
                    id: piece
                    objectName: "piece";
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    width: 48
                    height: 48
                }
                MouseArea {
                    id: mouse_rea
                    hoverEnabled: true
                    anchors.fill: parent
                    onClicked: square.qmlSquareClicked();
                    onEntered: if (square.state != 'selected') square.state = 'hovered';
                    onExited:  if (square.state == 'hovered') square.state = 'default';
                }

                states: [
                    State {
                        name: "selected"
                        PropertyChanges { target: square;
                                          color: Qt.rgba(0, 0.5, 0, 0.5)
                                          border.color: Qt.rgba(0, 0.5, 0, 0.8)
                        }
                    },
                    State {
                        name: "hovered"
                        PropertyChanges { target: square;
                                          color: Qt.rgba(1, 1, 1, 0.5)
                                          border.color: Qt.rgba(1, 1, 1, 0.8)
                        }
                    },
                    State {
                        name: "default"
                        PropertyChanges { target: square;
                                          color: "transparent"
                                          border.color: "transparent"
                        }
                    }
                ]
            }
        }
        Repeater {
            model: 64
            objectName: "square_repeater"
            Loader { sourceComponent: square_component}
        }
    }
    Rectangle {
        id: game_over
        objectName: "game_over";
        visible: false
        color: Qt.rgba(1, 1, 1, 0.6)
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.fill: parent
        Text {
            text: "Game Over"
            y: 30
            anchors.horizontalCenter: game_over.horizontalCenter
            anchors.verticalCenter: game_over.verticalCenter
            font.pointSize: 24; font.bold: true
        }
        MouseArea {
            id: mouse_area
            anchors.fill: parent
        }
    }
}
