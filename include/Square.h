#include <QPushButton>

class Piece;
class Board;

class Square: public QObject
{
Q_OBJECT

public:
    Square(int x, int y, bool is_playable, Board* board, QObject *square_component);
    ~Square();

    Piece * setPiece(Piece * piece);
    Piece * getPiece() const;

    void setSelected(bool isSelected);

    int getX() const;
    int getY() const;

public slots:
    void onSquareClicked();

private:
    static Square* m_square; //currently selected square
    Piece *m_piece;

    int m_x, m_y;
    Board *m_board;
    QObject *m_square_component;
};
