#include <Pieces/PieceType.h>
#include <QObject>
#include <list>

class Square;
class Piece;
struct MoveInfo;

class Board: public QObject
{
Q_OBJECT

public:
    Board(QList<QObject *> squares, const char *res_dir_path, bool is_playable);
    ~Board();

    void startNewGame();
    void loadGameHistory(const char *path);
    void saveGameHistory(const char *path);

    void nextMove();
    void prevMove();

    const Square* getSquare(int x, int y) const;
    bool checkPath(int pathLenght, int fromX, int fromY, int dX, int dY) const;

    void changeTurn(Square *from, Square *to, Piece *moved_piece, Piece *killed_piece, bool is_promotion = false);
    bool getIsWhiteTurn() const;

    Piece* promotePawn();

signals:
    void pieceMoved();
    void saveGameDone(bool is_successful);
    void loadGameDone(bool is_successful);

    void nextMoveDone(int moves_left);
    void prevMoveDone(int moves_left);

    void gameOver(bool is_white_turn);

private:
    Piece *getPiece(PieceType type, bool is_white) const;

    bool is_white_turn;
    bool m_is_playable;
    Square*  m_board[8][8];
    Piece* m_pieces_vector[12];

    std::list<MoveInfo *> m_moves_list;
    std::list<MoveInfo *>::iterator m_current_move;
};
