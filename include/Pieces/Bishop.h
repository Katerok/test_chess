#ifndef H_BISHOP
#define H_BISHOP

#include <Pieces/Piece.h>

class Board;

class Bishop: public Piece
{
public:
    Bishop(const char *res_part, bool is_white);
    virtual bool moveTo(int from_x, int from_y, int to_x, int to_y, Board* board);
};

#endif
