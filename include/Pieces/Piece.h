#ifndef H_PIECE
#define H_PIECE

#include <QIcon>
#include <Pieces/PieceType.h>

class Board;

class Piece
{
protected:
    QString m_image_path;
    bool   m_is_white;
    PieceType m_type;

    bool getMoveDeltas(int fromX, int fromY, int toX, int toY, int *dX, int *dY) const;

public:
    Piece(const char *res_part, PieceType type, bool is_white);
    virtual ~Piece();

    virtual bool moveTo(int from_x, int from_y, int to_x, int to_y, Board* board) = 0;
    bool getIsWhite() const;
    bool isSameColor(const Piece *other_piece) const;
    const QString& getImagePath() const;

    PieceType getPieceType() const;
};

#endif
