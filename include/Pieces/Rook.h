#ifndef H_ROOK
#define H_ROOK

#include <Pieces/Piece.h>

class Board;

class Rook: public Piece
{
public:
    Rook(const char *res_part, bool is_white);
    virtual bool moveTo(int from_x, int from_y, int to_x, int to_y, Board* board);
};

#endif
