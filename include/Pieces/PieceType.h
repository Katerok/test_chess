#ifndef H_PIECE_TYPE
#define H_PIECE_TYPE

typedef enum
{
    KING = 0,
    QUEEN,
    BISHOP,
    KNIGHT,
    ROOK,
    PAWN
} PieceType;

#endif
