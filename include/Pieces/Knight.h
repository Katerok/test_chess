#ifndef H_KNIGHT
#define H_KNIGHT

#include <Pieces/Piece.h>

class Board;

class Knight: public Piece
{
public:
    Knight(const char *res_part, bool is_white);
    virtual bool moveTo(int from_x, int from_y, int to_x, int to_y, Board* board);
};

#endif
