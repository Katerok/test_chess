#ifndef H_KING
#define H_KING

#include <Pieces/Piece.h>

class Board;

class King: public Piece
{
public:
    King(const char *res_part, bool is_white);
    virtual bool moveTo(int from_x, int from_y, int to_x, int to_y, Board* board);
};

#endif
