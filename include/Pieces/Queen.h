#ifndef H_QUEEN
#define H_QUEEN

#include <Pieces/Piece.h>

class Board;

class Queen: public Piece
{
public:
    Queen(const char *res_part, bool is_white);
    virtual bool moveTo(int from_x, int from_y, int to_x, int to_y, Board* board);
};

#endif
