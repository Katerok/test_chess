#include "ui_PromotionDialog.h"
#include <Pieces/PieceType.h>

class PromotionDialog : public QDialog
{
Q_OBJECT

public:
    PromotionDialog(QWidget *parent = 0);
    PieceType getSelectedPieceType();

private slots:
    void onPromoteToQueen();
    void onPromoteToRook();
    void onPromoteToKnight();
    void onPromoteToBishop();

private:
    Ui::PromotionDialog ui;
    PieceType selected_type;
};
