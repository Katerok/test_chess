#include "ui_MainWindow.h"

class QQuickView;
class Board;
class QQuickItem;

class MainWindow : public QWidget
{
Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onNewGame();
    void onLoadGame();
    void onSaveGame();
    void onStopGame();

    void onPieceMoved();
    void onSaveGameDone(bool is_successful);
    void onLoadGameDone(bool is_successful);

    void onNextMove();
    void onPrevMove();
    void onNextMoveDone(int moves_left);
    void onPrevMoveDone(int moves_left);

    void onGameOver(bool is_white_turn);

private:
    bool initializeBoard(bool is_playable);
    void showGameOverItem(bool is_visible);
    void getAllSquareItems(QQuickItem *parent, QList<QObject *> &list);

    QQuickView *m_view;
    Ui::MainWindow ui;
    Board *m_board;
};
