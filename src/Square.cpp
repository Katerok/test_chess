#include <Square.h>
#include <Board.h>
#include <QQuickView>

#include <Pieces/Piece.h>

Square* Square::m_square = NULL;

Square::Square(int x, int y, bool is_playable, Board* board, QObject *square_component):
    m_piece(NULL),
    m_x(x), m_y(y),
    m_board(board),
    m_square_component(square_component)
{
    setSelected(false);

    if (is_playable)
        connect(m_square_component, SIGNAL(qmlSquareClicked()),
                this, SLOT(onSquareClicked()));
}

Square::~Square()
{
}

Piece* Square::setPiece(Piece *piece)
{
    Piece *ret = m_piece;

    QObject *piece_obj = m_square_component->findChild<QObject*>("piece");

    if (piece)
    {
        piece_obj->setProperty("source", QUrl(piece->getImagePath()));
        piece_obj->setProperty("visible", true);
    }
    else
        piece_obj->setProperty("visible", false);
    m_piece = piece;
    return ret;
}

Piece* Square::getPiece() const
{
    return m_piece;
}

void Square::setSelected(bool isSelected)
{
    if (isSelected)
        m_square_component->setProperty("state", "selected");
    else
        m_square_component->setProperty("state", "default");
}

void Square::onSquareClicked()
{
    if (Square::m_square == this)
        return;

    if (Square::m_square)
    {
        if (Square::m_square->getPiece()->moveTo(Square::m_square->getX(),
                                                 Square::m_square->getY(),
                                                 m_x, m_y, m_board) &&
            !Square::m_square->getPiece()->isSameColor(m_piece))
        {
            // Check if pawn promotion
            if ((Square::m_square->getPiece()->getPieceType() == PAWN) &&
                ((m_x == 0) || (m_x == 7)) && (m_piece->getPieceType() != KING))
            {
                Piece * new_piece = m_board->promotePawn();
                m_board->changeTurn(Square::m_square, this,
                                    Square::m_square->getPiece(), new_piece,
                                    true);
                setPiece(new_piece);
            }
            else
            {
                m_board->changeTurn(Square::m_square, this,
                                    Square::m_square->getPiece(), m_piece);
                setPiece(Square::m_square->getPiece());
            }
            Square::m_square->setPiece(NULL);
        }

        Square::m_square->setSelected(false);
        Square::m_square = NULL;
    }
    else if (m_piece && (m_board->getIsWhiteTurn() == m_piece->getIsWhite()))
    {
        Square::m_square = this;
        setSelected(true);
    }
}

int Square::getX() const
{
    return m_x;
}

int Square::getY() const
{
    return m_y;
}

