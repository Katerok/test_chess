#include <PromotionDialog.h>
#include <Pieces/PieceType.h>

PromotionDialog::PromotionDialog(QWidget *parent):
    QDialog(parent),
    selected_type(QUEEN)
{
    ui.setupUi(this);
    connect(ui.bt_queen, SIGNAL(clicked()), this, SLOT(onPromoteToQueen()));
    connect(ui.bt_bishop, SIGNAL(clicked()), this, SLOT(onPromoteToBishop()));
    connect(ui.bt_knight, SIGNAL(clicked()), this, SLOT(onPromoteToKnight()));
    connect(ui.bt_rook, SIGNAL(clicked()), this, SLOT(onPromoteToRook()));
}

void PromotionDialog::onPromoteToQueen()
{
    selected_type = QUEEN;
    close();
}

void PromotionDialog::onPromoteToRook()
{
    selected_type = ROOK;
    close();
}

void PromotionDialog::onPromoteToKnight()
{
    selected_type = KNIGHT;
    close();
}

void PromotionDialog::onPromoteToBishop()
{
    selected_type = BISHOP;
    close();
}

PieceType PromotionDialog::getSelectedPieceType()
{
    return selected_type;
}

