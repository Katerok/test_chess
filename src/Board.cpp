#include <Square.h>
#include <Board.h>

#include <Pieces/Knight.h>
#include <Pieces/King.h>
#include <Pieces/Rook.h>
#include <Pieces/Pawn.h>
#include <Pieces/Bishop.h>
#include <Pieces/Queen.h>

#include <PromotionDialog.h>

#include <fstream>

struct MoveInfo
{
    MoveInfo(Square *from, Square *to, Piece *moved_piece, Piece *killed_piece, bool is_promotion):
        m_from(from),
        m_to(to),
        m_moved_piece(moved_piece),
        m_killed_piece(killed_piece),
        m_is_promotion(is_promotion)
    {
    }

    Square *m_from;
    Square *m_to;
    Piece *m_moved_piece;
    Piece *m_killed_piece;
    bool m_is_promotion;
};

Board::Board(QList<QObject *> squares, const char *res_dir_path, bool is_playable):
    m_is_playable(is_playable)
{
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            m_board[i][j] = new Square(i, j, is_playable,
                                       this, squares.at(i * 8 + j));

    m_pieces_vector[0] = new King(res_dir_path, true);
    m_pieces_vector[1] = new King(res_dir_path, false);
    m_pieces_vector[2] = new Queen(res_dir_path, true);
    m_pieces_vector[3] = new Queen(res_dir_path, false);
    m_pieces_vector[4] = new Bishop(res_dir_path, true);
    m_pieces_vector[5] = new Bishop(res_dir_path, false);
    m_pieces_vector[6] = new Knight(res_dir_path, true);
    m_pieces_vector[7] = new Knight(res_dir_path, false);
    m_pieces_vector[8] = new Rook(res_dir_path, true);
    m_pieces_vector[9] = new Rook(res_dir_path, false);
    m_pieces_vector[10] = new Pawn(res_dir_path, true);
    m_pieces_vector[11] = new Pawn(res_dir_path, false);
}

Board::~Board()
{
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
    {
        m_board[i][j]->setPiece(NULL);
        delete m_board[i][j];
    }

    for (int i = 0; i < 12; i++)
        delete m_pieces_vector[i];

    for (std::list<MoveInfo *>::iterator it=m_moves_list.begin();
         it != m_moves_list.end(); ++it)
        delete *it;
}

Piece* Board::getPiece(PieceType type, bool is_white)  const
{
    return m_pieces_vector[type * 2 + !is_white];
}

void Board::startNewGame()
{
    is_white_turn = true;
    m_board[7][0]->setPiece(getPiece(ROOK, true));
    m_board[7][7]->setPiece(getPiece(ROOK, true));
    m_board[0][0]->setPiece(getPiece(ROOK, false));
    m_board[0][7]->setPiece(getPiece(ROOK, false));
    m_board[7][1]->setPiece(getPiece(KNIGHT, true));
    m_board[7][6]->setPiece(getPiece(KNIGHT, true));
    m_board[0][1]->setPiece(getPiece(KNIGHT, false));
    m_board[0][6]->setPiece(getPiece(KNIGHT, false));
    m_board[7][2]->setPiece(getPiece(BISHOP, true));
    m_board[7][5]->setPiece(getPiece(BISHOP, true));
    m_board[0][2]->setPiece(getPiece(BISHOP, false));
    m_board[0][5]->setPiece(getPiece(BISHOP, false));
    m_board[7][3]->setPiece(getPiece(QUEEN, true));
    m_board[0][3]->setPiece(getPiece(QUEEN, false));
    m_board[0][4]->setPiece(getPiece(KING, false));
    m_board[7][4]->setPiece(getPiece(KING, true));

    for (int i = 0; i < 8; i++)
    {
        m_board[1][i]->setPiece(getPiece(PAWN, false));
        m_board[6][i]->setPiece(getPiece(PAWN, true));
    }
}

const Square* Board::getSquare(int x, int y) const
{
    return m_board[x][y];
}

bool Board::checkPath(int pathLenght, int fromX, int fromY, int dX, int dY) const
{
    for (int i = 0; i < pathLenght; i++,
                                    fromX = fromX + dX,
                                    fromY = fromY + dY)
    {
        if(m_board[fromX][fromY]->getPiece())
            return false;
    }
    return true;
}

void Board::changeTurn(Square *from, Square *to, Piece *moved_piece, Piece *killed_piece, bool is_promotion)
{
    MoveInfo *move = new MoveInfo(from, to,
                                  moved_piece, killed_piece, is_promotion);
    m_moves_list.push_back(move);

    emit pieceMoved();

    if (killed_piece && (killed_piece->getPieceType() == KING))
        emit gameOver(is_white_turn);

    is_white_turn = !is_white_turn;
}

bool Board::getIsWhiteTurn() const
{
    return is_white_turn;
}

void Board::saveGameHistory(const char *path)
{
    std::ofstream ofs(path, std::ofstream::out);
    if (!ofs)
        emit saveGameDone(false);

    for (std::list<MoveInfo *>::iterator it=m_moves_list.begin();
         it != m_moves_list.end(); ++it)
    {
        if (!(*it)->m_is_promotion)
            ofs.put('-');
        else
            ofs.put((*it)->m_killed_piece->getPieceType() + '0');

        ofs.put(8 - (*it)->m_from->getY() + 'a');
        ofs.put(8 - (*it)->m_from->getX() + '0');
        ofs.put('-');
        ofs.put(8 - (*it)->m_to->getY() + 'a');
        ofs.put(8 - (*it)->m_to->getX() + '0');
        ofs.put('\n');
    }

    emit saveGameDone(!ofs.fail());
    ofs.close();
}

void Board::loadGameHistory(const char *path)
{
    std::ifstream ifs(path);
    if (!ifs)
        emit loadGameDone(false);

    // string to keep line like '-e2-e4\n' and corresponding regexp
    char move[7];
    QRegExp regexp("^(\\-|[1-4])[a-h][1-8]\\-[a-h][1-8]$");

    int fromY, fromX, toY, toX;
    bool is_white_turn = true;
    while (!ifs.eof() && ifs.getline(move, 7))
    {
        if (!regexp.exactMatch(move))
        {
            emit loadGameDone(false);
            ifs.close();
            return;
        }

        fromY = 8 - move[1] +'a';
        fromX = 8 - move[2] +'0';
        toY = 8 - move[4] +'a';
        toX = 8 - move[5] +'0';

        Piece* moved_piece = NULL;

        if (move[0] != '-')
            moved_piece = getPiece(static_cast<PieceType>(move[0] - '0'),
                                   is_white_turn);

        m_moves_list.push_back(new MoveInfo(m_board[fromX][fromY],
                                            m_board[toX][toY],
                                            moved_piece, NULL,
                                            move[0] != '-'));

        is_white_turn = !is_white_turn;
    }

    m_current_move = m_moves_list.begin();
    emit loadGameDone(ifs.eof() && m_moves_list.size());
    ifs.close();
}

void Board::nextMove()
{
    if (m_is_playable || m_current_move == m_moves_list.end())
        return;

    MoveInfo *current_move = *m_current_move;

    if (!current_move->m_from->getPiece() ||
        (current_move->m_from->getPiece()->getIsWhite() != is_white_turn))
    {
        emit nextMoveDone(-1);
        return;
    }

    if (current_move->m_is_promotion)
    {
        current_move->m_killed_piece = current_move->m_from->getPiece();
        current_move->m_from->setPiece(NULL);
        current_move->m_to->setPiece(current_move->m_moved_piece);
    }
    else
    if (current_move->m_from->getPiece()->moveTo(current_move->m_from->getX(),
                                                 current_move->m_from->getY(),
                                                 current_move->m_to->getX(),
                                                 current_move->m_to->getY(),
                                                 this) &&
        !current_move->m_from->getPiece()->isSameColor(current_move->m_to->getPiece()))
    {
        current_move->m_killed_piece = current_move->m_to->getPiece();
        current_move->m_to->setPiece(current_move->m_from->getPiece());
        current_move->m_from->setPiece(NULL);
    }
    else
    {
        emit nextMoveDone(-1);
        return;
    }

    is_white_turn = !is_white_turn;
    m_current_move++;

    emit nextMoveDone(m_current_move != m_moves_list.end());
}

void Board::prevMove()
{
    if (m_is_playable || m_current_move == m_moves_list.begin())
        return;

    m_current_move--;

    MoveInfo *current_move = *m_current_move;

    if (current_move->m_is_promotion)
    {
        current_move->m_to->setPiece(NULL);
        current_move->m_from->setPiece(current_move->m_killed_piece);
    }
    else
    {
        current_move->m_from->setPiece(current_move->m_to->getPiece());
        current_move->m_to->setPiece(current_move->m_killed_piece);
    }

    is_white_turn = !is_white_turn;

    emit prevMoveDone(m_current_move != m_moves_list.begin());
}

Piece* Board::promotePawn()
{
    PromotionDialog promotion_dialog;
    promotion_dialog.exec();

    return getPiece(promotion_dialog.getSelectedPieceType(), is_white_turn);
}

