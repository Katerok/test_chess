#include <Pieces/Pawn.h>
#include <Board.h>
#include <Square.h>

Pawn::Pawn(const char *res_part, bool is_white):
    Piece(res_part, PAWN, is_white)
{
}

bool Pawn::moveTo(int fromX,int fromY,int toX,int toY, Board* board)
{
    int dx(-1), init_row(1);

    if (m_is_white)
    {
        dx = 1;
        init_row = 6;
    }

    // attack move
    if ((std::abs(fromY-toY) == 1) && (fromX-toX == dx) &&
             board->getSquare(toX, toY)->getPiece())
        return true;

    // move forvard
    if (fromY == toY)
    {
        if ((fromX-toX == dx) && !board->getSquare(toX, toY)->getPiece())
            return true;
        else
            if ((fromX == init_row) && (toX == init_row - 2 * dx) &&
                !board->getSquare(init_row - dx, toY)->getPiece() &&
                !board->getSquare(init_row - 2 * dx, toY)->getPiece())
                return true;
    }

    return false;
}

