#include <Pieces/Queen.h>
#include <Board.h>

Queen::Queen(const char *res_part, bool is_white):
    Piece(res_part, QUEEN, is_white)
{
}

bool Queen::moveTo(int fromX,int fromY,int toX,int toY, Board* board)
{
    int dX, dY;
    if (!getMoveDeltas(fromX, fromY, toX, toY, &dX, &dY))
        return false;

    board->checkPath(std::abs(fromX - toX) - 1, fromX, fromY, dX, dY);

    return true;
}

