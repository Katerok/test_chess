#include <Pieces/Piece.h>
#include <Square.h>

Piece::Piece(const char *res_part, PieceType type, bool is_white):
    m_is_white(is_white),
    m_type(type)
{
    m_image_path.append("file://");
    m_image_path.append(res_part);
    m_image_path.append("/");
    switch (type)
    {
        case PAWN:
            m_image_path.append("pawn");
            break;
        case KNIGHT:
            m_image_path.append("knight");
            break;
        case KING:
            m_image_path.append("king");
            break;
        case QUEEN:
            m_image_path.append("queen");
            break;
        case ROOK:
            m_image_path.append("rook");
            break;
        case BISHOP:
            m_image_path.append("bishop");
            break;
    };

    if (m_is_white)
        m_image_path.append("_white.png");
    else
        m_image_path.append("_black.png");
}

Piece::~Piece()
{
}

const QString& Piece::getImagePath() const
{
    return m_image_path;
}

bool Piece::getIsWhite() const
{
    return m_is_white;
}

bool Piece::isSameColor(const Piece *other_piece) const
{
    if (!other_piece)
        return false;

    return m_is_white == other_piece->getIsWhite();
}

bool Piece::getMoveDeltas(int fromX, int fromY, int toX, int toY, int *dX, int *dY) const
{
    if (!dX || !dY)
        return false;

    int step_x = toX - fromX;
    int step_y = toY - fromY;

    // horizontal or vertical movement
    if (!step_x || !step_y)
    {
        *dX = (step_x) ? (step_x / std::abs(step_x)) : 0;
        *dY = (step_y) ? (step_y / std::abs(step_y)) : 0;
        return true;
    }
    // diagonal movement (steps are not 0 for sure)
    if (std::abs(step_x) == std::abs(step_y))
    {
        *dX = (step_x) / std::abs(step_x);
        *dY = (step_y) / std::abs(step_y);
        return true;
    }

    return false;
}

PieceType Piece::getPieceType() const
{
    return m_type;
}

