#include <Pieces/Rook.h>
#include <Board.h>

Rook::Rook(const char *res_part, bool is_white):
    Piece(res_part, ROOK, is_white)
{
}

bool Rook::moveTo(int fromX,int fromY,int toX,int toY, Board* board)
{
    int dX, dY;
    if (!getMoveDeltas(fromX, fromY, toX, toY, &dX, &dY) || (dX && dY))
        return false;

    board->checkPath(std::abs(fromX - toX) - 1, fromX, fromY, dX, dY);

    return true;
}

