#include <Pieces/King.h>
#include <Board.h>

King::King(const char *res_part, bool is_white):
    Piece(res_part, KING, is_white)
{
}

bool King::moveTo(int fromX,int fromY,int toX,int toY, Board*)
{
    if (std::abs(fromX-toX) <= 1 && std::abs(fromY-toY) <=1)
        return true;

    return false;
}

