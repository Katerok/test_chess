#include <Pieces/Knight.h>
#include <Board.h>

#include <iostream>

Knight::Knight(const char *res_part, bool is_white):
    Piece(res_part, KNIGHT, is_white)
{
}

bool Knight::moveTo(int fromX, int fromY, int toX, int toY, Board*)
{
    if (std::abs(fromX - toX) + std::abs(fromY - toY) == 3)
        return true;

    return false;
}

