#include <MainWindow.h>

#include <QQuickItem>
#include <QQuickView>
#include <QFileDialog>
#include <QMessageBox>

#include <Board.h>

MainWindow::MainWindow(QWidget *parent):
    QWidget(parent),
    m_board(NULL)
{
    ui.setupUi(this);

    m_view = new QQuickView();
    ui.gl_square_layout->addWidget(QWidget::createWindowContainer(m_view));

    QString filePath = QApplication::applicationDirPath();
    filePath.append("/res/board.qml");
    m_view->setSource(QUrl(filePath));

    connect(ui.bt_new_game, SIGNAL(clicked()), this, SLOT(onNewGame()));
    connect(ui.bt_save_game, SIGNAL(clicked()), this, SLOT(onSaveGame()));
    connect(ui.bt_load_game, SIGNAL(clicked()), this, SLOT(onLoadGame()));
    connect(ui.bt_stop_game, SIGNAL(clicked()), this, SLOT(onStopGame()));
}

MainWindow::~MainWindow()
{
    if (m_board)
        delete m_board;
    delete m_view;
}

void MainWindow::onNewGame()
{
    if (!initializeBoard(true))
        return;

    m_board->startNewGame();

    ui.bt_stop_game->setEnabled(true);
    ui.bt_load_game->setEnabled(false);
    ui.bt_new_game->setEnabled(false);
    ui.bt_next_move->setEnabled(false);
    ui.bt_prev_move->setEnabled(false);

    connect(m_board, SIGNAL(pieceMoved()), this, SLOT(onPieceMoved()));
    connect(m_board, SIGNAL(saveGameDone(bool)),
            this, SLOT(onSaveGameDone(bool)));
    connect(m_board, SIGNAL(gameOver(bool)), this, SLOT(onGameOver(bool)));
}

void MainWindow::onStopGame()
{
    if (m_board)
    {
        delete m_board;
        m_board = NULL;
    }

    ui.bt_stop_game->setEnabled(false);
    ui.bt_save_game->setEnabled(false);
    ui.bt_new_game->setEnabled(true);
    ui.bt_load_game->setEnabled(true);
    ui.bt_next_move->setEnabled(false);
    ui.bt_prev_move->setEnabled(false);

    showGameOverItem(false);
}

void MainWindow::onSaveGame()
{
    if (!m_board)
        return;

    QString fn = QFileDialog::getSaveFileName(this, tr("Save as..."),
                                              QString(),
                                              tr("Test Chess Files (*.tc)"));

    if (fn.isEmpty())
        return;

    if (!fn.contains(QRegExp(".+(\\.tc$)", Qt::CaseInsensitive)))
        fn.append(".tc");

    m_board->saveGameHistory(fn.toUtf8().constData());
}

void MainWindow::onLoadGame()
{
    QString fn = QFileDialog::getOpenFileName(this, tr("Load file"),
                                              QString(),
                                              tr("Test Chess Files (*.tc)"));

    if (fn.isEmpty())
        return;

    if (!initializeBoard(false))
        return;

    connect(m_board, SIGNAL(loadGameDone(bool)),
            this, SLOT(onLoadGameDone(bool)));

    m_board->loadGameHistory(fn.toUtf8().constData());
}

void MainWindow::getAllSquareItems(QQuickItem *parent, QList<QObject *> &list)
{
    QList<QQuickItem *> children = parent->childItems();

    foreach (QQuickItem *item, children)
    {
        QObject *searchObject = dynamic_cast<QObject *>(item);
        if (searchObject->objectName() == "square")
            list.append(searchObject);
        // call recursively and browse through the whole scene graph
        getAllSquareItems(item, list);
    }
}

bool MainWindow::initializeBoard(bool is_playable)
{
    if (m_board)
        delete m_board;

    QString filePath = QApplication::applicationDirPath();
    filePath.append("/res");

    QQuickItem *object = m_view->rootObject();
    QList<QObject *> list;
    getAllSquareItems(object, list);

    if (list.size() != 64)
    {
        QMessageBox::critical(this, tr("Error"),
                              tr("Something went wrong with board layout"));
        return false;
    }
    else
        m_board = new Board(list, filePath.toStdString().c_str(),
                            is_playable);

    return true;
}

void MainWindow::onPieceMoved()
{
    ui.bt_save_game->setEnabled(true);
    disconnect(m_board, SIGNAL(pieceMoved()), this, SLOT(onPieceMoved()));
}

void MainWindow::onSaveGameDone(bool is_successful)
{
    if (is_successful)
    {
        QMessageBox::information(this, tr("Save Game"),
                                 tr("Game is saved successfully"));
    }
    else
    {
        QMessageBox::critical(this, tr("Save Game"),
                              tr("Game is not saved"));
    }
}

void MainWindow::onLoadGameDone(bool is_successful)
{
    if (!is_successful)
    {
        QMessageBox::critical(this, tr("Load Game"),
                              tr("Game is not loaded"));

        onStopGame();
    }
    else
    {
        m_board->startNewGame();
        ui.bt_next_move->setEnabled(true);
        connect(ui.bt_next_move, SIGNAL(clicked()),
                this, SLOT(onNextMove()), Qt::UniqueConnection);
        connect(m_board, SIGNAL(nextMoveDone(int)),
                this, SLOT(onNextMoveDone(int)), Qt::UniqueConnection);
        connect(ui.bt_prev_move, SIGNAL(clicked()),
                this, SLOT(onPrevMove()), Qt::UniqueConnection);
        connect(m_board, SIGNAL(prevMoveDone(int)),
                this, SLOT(onPrevMoveDone(int)), Qt::UniqueConnection);
    }
}

void MainWindow::onNextMove()
{
    m_board->nextMove();
}

void MainWindow::onNextMoveDone(int moves_left)
{
    if (moves_left < 0)
    {
        QMessageBox::critical(this, tr("Next Move"),
                              tr("Can not reproduce next move."));

        onStopGame();
    }
    else
    {
        if (moves_left == 0)
            ui.bt_next_move->setEnabled(false);
        ui.bt_prev_move->setEnabled(true);
    }
}

void MainWindow::onPrevMove()
{
    m_board->prevMove();
}

void MainWindow::onPrevMoveDone(int moves_left)
{
    if (moves_left == 0)
        ui.bt_prev_move->setEnabled(false);
    ui.bt_next_move->setEnabled(true);
}

void MainWindow::onGameOver(bool)
{
    showGameOverItem(true);
}

void MainWindow::showGameOverItem(bool is_visible)
{
    QQuickItem *object = m_view->rootObject();
    QObject *game_over = object->findChild<QObject*>("game_over");
    if (game_over)
        game_over->setProperty("visible", is_visible);
}

